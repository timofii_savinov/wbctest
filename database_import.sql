-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 27 2020 г., 14:16
-- Версия сервера: 5.6.43-log
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `wbc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Dashboard', 'fa-bar-chart', '/', NULL, NULL, NULL),
(2, 0, 2, 'Admin', 'fa-tasks', '', NULL, NULL, NULL),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, NULL, NULL),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, NULL),
(5, 2, 5, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, NULL),
(6, 2, 6, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, NULL),
(7, 2, 7, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, NULL),
(8, 0, 0, 'Hello link', 'fa-angellist', 'test', '*', '2020-08-25 09:39:27', '2020-08-25 09:46:11');

-- --------------------------------------------------------

--
-- Структура таблицы `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-23 05:53:11', '2020-08-23 05:53:11'),
(2, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 09:29:10', '2020-08-25 09:29:10'),
(3, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 09:30:15', '2020-08-25 09:30:15'),
(4, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 09:30:58', '2020-08-25 09:30:58'),
(5, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:31:01', '2020-08-25 09:31:01'),
(6, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:31:03', '2020-08-25 09:31:03'),
(7, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:31:08', '2020-08-25 09:31:08'),
(8, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:31:11', '2020-08-25 09:31:11'),
(9, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:31:50', '2020-08-25 09:31:50'),
(10, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:32:30', '2020-08-25 09:32:30'),
(11, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:32:32', '2020-08-25 09:32:32'),
(12, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:32:35', '2020-08-25 09:32:35'),
(13, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:37:42', '2020-08-25 09:37:42'),
(14, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"New menu link\",\"icon\":\"fa-angellist\",\"uri\":\"hellowbc\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"MfqXzlYMwsZyxdjB8cKm39FnEXEDxNAqUZR3RykO\"}', '2020-08-25 09:39:26', '2020-08-25 09:39:26'),
(15, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-08-25 09:39:27', '2020-08-25 09:39:27'),
(16, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-08-25 09:39:33', '2020-08-25 09:39:33'),
(17, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:39:59', '2020-08-25 09:39:59'),
(18, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2020-08-25 09:40:29', '2020-08-25 09:40:29'),
(19, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:40:33', '2020-08-25 09:40:33'),
(20, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:40:34', '2020-08-25 09:40:34'),
(21, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:40:38', '2020-08-25 09:40:38'),
(22, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-08-25 09:42:14', '2020-08-25 09:42:14'),
(23, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-08-25 09:42:25', '2020-08-25 09:42:25'),
(24, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:42:46', '2020-08-25 09:42:46'),
(25, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:42:54', '2020-08-25 09:42:54'),
(26, 1, 'admin/auth/menu/8', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Hello link\",\"icon\":\"fa-angellist\",\"uri\":\"hellowbc\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"MfqXzlYMwsZyxdjB8cKm39FnEXEDxNAqUZR3RykO\",\"after-save\":\"3\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-08-25 09:43:17', '2020-08-25 09:43:17'),
(27, 1, 'admin/auth/menu/8', 'GET', '127.0.0.1', '[]', '2020-08-25 09:43:18', '2020-08-25 09:43:18'),
(28, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '[]', '2020-08-25 09:43:18', '2020-08-25 09:43:18'),
(29, 1, 'admin/auth/menu/8', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:43:28', '2020-08-25 09:43:28'),
(30, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '[]', '2020-08-25 09:43:28', '2020-08-25 09:43:28'),
(31, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:43:30', '2020-08-25 09:43:30'),
(32, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-08-25 09:44:14', '2020-08-25 09:44:14'),
(33, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:45:56', '2020-08-25 09:45:56'),
(34, 1, 'admin/auth/menu/8', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Hello link\",\"icon\":\"fa-angellist\",\"uri\":\"test\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"MfqXzlYMwsZyxdjB8cKm39FnEXEDxNAqUZR3RykO\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-08-25 09:46:11', '2020-08-25 09:46:11'),
(35, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-08-25 09:46:11', '2020-08-25 09:46:11'),
(36, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-08-25 09:46:20', '2020-08-25 09:46:20'),
(37, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 09:48:27', '2020-08-25 09:48:27'),
(38, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 09:49:22', '2020-08-25 09:49:22'),
(39, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 09:49:31', '2020-08-25 09:49:31'),
(40, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 09:52:29', '2020-08-25 09:52:29'),
(41, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 09:53:11', '2020-08-25 09:53:11'),
(42, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 09:55:47', '2020-08-25 09:55:47'),
(43, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 10:00:00', '2020-08-25 10:00:00'),
(44, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 10:01:02', '2020-08-25 10:01:02'),
(45, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:01:07', '2020-08-25 10:01:07'),
(46, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 10:01:08', '2020-08-25 10:01:08'),
(47, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 10:02:42', '2020-08-25 10:02:42'),
(48, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:02:47', '2020-08-25 10:02:47'),
(49, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 10:02:54', '2020-08-25 10:02:54'),
(50, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 10:04:48', '2020-08-25 10:04:48'),
(51, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:04:53', '2020-08-25 10:04:53'),
(52, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:05:02', '2020-08-25 10:05:02'),
(53, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:05:06', '2020-08-25 10:05:06'),
(54, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:05:07', '2020-08-25 10:05:07'),
(55, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:05:09', '2020-08-25 10:05:09'),
(56, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:05:12', '2020-08-25 10:05:12'),
(57, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:05:16', '2020-08-25 10:05:16'),
(58, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:05:21', '2020-08-25 10:05:21'),
(59, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:05:25', '2020-08-25 10:05:25'),
(60, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:05:46', '2020-08-25 10:05:46'),
(61, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-08-25 10:06:26', '2020-08-25 10:06:26'),
(62, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:06:37', '2020-08-25 10:06:37'),
(63, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:07:26', '2020-08-25 10:07:26'),
(64, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:09:13', '2020-08-25 10:09:13'),
(65, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"30\"}', '2020-08-25 10:09:20', '2020-08-25 10:09:20'),
(66, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"50\"}', '2020-08-25 10:09:22', '2020-08-25 10:09:22'),
(67, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:09:26', '2020-08-25 10:09:26'),
(68, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:09:29', '2020-08-25 10:09:29'),
(69, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:09:31', '2020-08-25 10:09:31'),
(70, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"per_page\":\"50\"}', '2020-08-25 10:09:35', '2020-08-25 10:09:35'),
(71, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:09:37', '2020-08-25 10:09:37'),
(72, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:09:39', '2020-08-25 10:09:39'),
(73, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:09:45', '2020-08-25 10:09:45'),
(74, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:09:46', '2020-08-25 10:09:46'),
(75, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:09:56', '2020-08-25 10:09:56'),
(76, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:12:45', '2020-08-25 10:12:45'),
(77, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:12:46', '2020-08-25 10:12:46'),
(78, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:12:53', '2020-08-25 10:12:53'),
(79, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:13:47', '2020-08-25 10:13:47'),
(80, 1, 'admin/test', 'GET', '127.0.0.1', '[]', '2020-08-25 10:14:17', '2020-08-25 10:14:17'),
(81, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:14:40', '2020-08-25 10:14:40'),
(82, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:14:44', '2020-08-25 10:14:44'),
(83, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:14:47', '2020-08-25 10:14:47'),
(84, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:14:50', '2020-08-25 10:14:50'),
(85, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:14:52', '2020-08-25 10:14:52'),
(86, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:14:54', '2020-08-25 10:14:54'),
(87, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:14:56', '2020-08-25 10:14:56'),
(88, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:16:51', '2020-08-25 10:16:51'),
(89, 1, 'admin/test', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-08-25 10:17:05', '2020-08-25 10:17:05');

-- --------------------------------------------------------

--
-- Структура таблицы `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2020-08-23 05:21:29', '2020-08-23 05:21:29');

-- --------------------------------------------------------

--
-- Структура таблицы `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 8, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$EtSkJrUe194ElwWHdPUySeReQb32DkX93DfSOcNtkMcTYBXoX6P5W', 'Administrator', NULL, 'sR2otJPQTGYe3UJtk87k9r0dYgpdxTVOymcQexdL9vofYdn20tK6859NnxIP', '2020-08-23 05:21:29', '2020-08-23 05:21:29');

-- --------------------------------------------------------

--
-- Структура таблицы `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_04_173148_create_admin_tables', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Индексы таблицы `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`),
  ADD UNIQUE KEY `admin_permissions_slug_unique` (`slug`);

--
-- Индексы таблицы `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`),
  ADD UNIQUE KEY `admin_roles_slug_unique` (`slug`);

--
-- Индексы таблицы `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Индексы таблицы `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Индексы таблицы `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Индексы таблицы `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- Индексы таблицы `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT для таблицы `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
