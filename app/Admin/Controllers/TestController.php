<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Table;

class TestController extends Controller
{
    public function index(Content $content)
    {
        return $content->withInfo('Test page for WBC', 'hello WBC!');


    }
}
